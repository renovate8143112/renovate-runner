module.exports = {
    endpoint: 'https://self-hosted.gitlab/api/v4/',
    token: process.env.RENOVATE_TOKEN,
    platform: 'gitlab',
    platformAutomerge: true,
    packageRules: [
        {
        "description": "Auto merge small changes as per https://docs.renovatebot.com/configuration-options/#automerge",
        "matchUpdateTypes": ["patch", "pin", "digest"],
        "automerge": true
        }
    ],
    onboardingConfig: {
      extends: ['Renovate/renovate-config'],
    },
    autodiscover: true
}